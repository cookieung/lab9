import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

/**
 * It's a word counter machine.
 * @author Salilthip 5710546640
 *
 */
public class WordCounter {
	State state;
	int syllables =0;
	static int total=0;

	/**
	 * Set the new state.
	 * @param newState is a new state.
	 */
	public void setState(State newState){
		if(newState != state) newState.enterState();
		this.state=newState;
	}

	/**
	 * For count the number of word from url.
	 * @param url is a source of String.
	 * @return the number of word
	 */
	public int countWords(URL url) {
		InputStream in =null;
		try {
			in = url.openStream( );
		} catch (IOException e) {

		}
		return countWords(in);
	}

	/**
	 * Get the total value of syllables.
	 * @return total of syllables.
	 */
	public int getSyllableCount(){
		return total;
	}

	/**
	 * For count the number of word from InputStream.
	 * @param inputStream is a source of string.
	 * @return the number of syllables.
	 */
	public int countWords(InputStream inputStream){
		int i=0;
		Scanner scanner = new Scanner(inputStream);

		while(scanner.hasNext()){
			i++;
			this.total += countSyllables(scanner.next());
		}

		return i;
	}
	
	/**
	 * For get the number of each word.
	 * @param word is a string of word.
	 * @return the number of this word.
	 */
	public int countSyllables( String word ) {
		syllables=0;
		state = START;
		word = word.replace("'", "");
		for( int c =0 ; c < word.length() ;c++ ) { 
			state.handlerChar(word.charAt(c));
		}
		if(state==DASH)syllables=0;
		if(state==VOWEL_STATE && isE(word.charAt(word.length()-1)) && syllables>1) syllables--;
		state = END;
		return syllables;
	}

	/**
	 * Check if it is a vowel.
	 * @param c is a character of one word.
	 * @return true if it's a vowel.
	 * 		false if it isn't a vowel.
	 */
	private boolean isVowel(char c){
		return ("AEIOUaeiou".indexOf(c)>=0);
	}
	
	/**
	 * Check if it is a letter.
	 * @param c is a character of one word.
	 * @return true if it's a letter.
	 * 		false if it isn't a letter.
	 */
	private boolean isLetter(char c){
		return Character.isLetter(c);
	}
	
	/**
	 * Check if it is the letter E.
	 * @param c is a character of one word.
	 * @return true if it's the letter E.
	 * 		false if it isn't the letter E.
	 */
	private boolean isE(char c){
		return (c=='e')||(c=='E');
	}
	
	/**
	 * Check if it is the letter Y.
	 * @param c is a character of one word.
	 * @return true if it's the letter Y.
	 * 		false if it isn't the letter Y.
	 */
	private boolean isY(char c){
		return (c=='y')||(c=='Y');
	}
	
	/**
	 * Check if it is Dash.
	 * @param c is a character of one word.
	 * @return true if it's Dash.
	 * 		false if it isn't Dash.
	 */
	private boolean isDash(char c){
		return (c=='-');
	}

	/**
	 * State of single vowel.
	 */
	State VOWEL_STATE = new State(){
		public void handlerChar(char c)
		{
			if(isVowel(c)) setState(MULTI_VOWEL);
			else if(isLetter(c)) setState(CONSONANT_STATE);
			else if(isDash(c))setState(DASH);
			else setState(NONWORD);
		}

		public void enterState(){
			syllables++;
		}
	};

	/**
	 * State of multiple vowel.
	 */
	State MULTI_VOWEL = new State() {

		@Override
		public void handlerChar(char c) {
			if(isVowel(c)) setState(MULTI_VOWEL);
			else if(isLetter(c)) setState(CONSONANT_STATE);
			else if(isDash(c)) setState(DASH);
			else setState(NONWORD);
		}

		@Override
		public void enterState() {
			//don't do anything
		}
	};
	
	/**
	 * State of Consonant.
	 */
	State CONSONANT_STATE = new State()
	{
		public void handlerChar(char c)
		{
			if(isVowel(c)) setState(VOWEL_STATE);
			else if(isY(c)) setState(VOWEL_STATE);
			else if(isLetter(c)) /*do nothing*/;
			else if(isDash(c)) setState(DASH);
			else setState(NONWORD);
		}

		@Override
		public void enterState() {
			// don't do anything
		}
	};
	
	/**
	 * Start State.
	 */
	State START = new State()
	{
		public void handlerChar(char c)
		{
			if(isVowel(c) || isY(c)) setState(VOWEL_STATE);
			else if(isLetter(c)) setState(CONSONANT_STATE);
			else if(isE(c)) setState(E_FIRST);
			else if(isNumber(c)) setState(NONWORD);
			else setState(NONWORD);

		}

		public boolean isNumber(char c){
			return Character.isDigit(c);
		}

		public void enterState(){
			//don't do anything.
		}
	};
	
	/**
	 * State of Dash.
	 */
	State DASH = new State() {

		@Override
		public void handlerChar(char c) {
			if(isDash(c)) /**/;
			else if(isVowel(c)) setState(VOWEL_STATE);
			else if(isY(c)) setState(VOWEL_STATE);
			else if(isLetter(c)) /*do nothing*/;
			else setState(NONWORD);
		}

		@Override
		public void enterState() {
			//don't do anything

		}
	};

	/**
	 * State of the first E.
	 */
	State E_FIRST = new State() {

		@Override
		public void handlerChar(char c) {
			if(isVowel(c))  setState(VOWEL_STATE);
			else if(isLetter(c)) setState(CONSONANT_STATE);
			else if(isDash(c)) setState(DASH);
			else setState(NONWORD);
		}

		@Override
		public void enterState() {
			if(syllables==0) syllables++;
		}
	};
	
	/**
	 * State of NONWORD.
	 */
	State NONWORD = new State() {

		@Override
		public void handlerChar(char c) {
		}

		@Override
		public void enterState() {

		}
	};
	
	/**
	 * END State.
	 */
	State END = new State() {

		@Override
		public void handlerChar(char c) {
			//don't do anything
		}

		@Override
		public void enterState() {
			//don't do anything
		}
	};

}

/**
 * Interface of State.
 *
 */
interface State{
	/**
	 * Count to be Syllable.
	 */
	public void enterState();
	/**
	 * Update the state
	 * @param c is the character in process.
	 */
	public void handlerChar(char c);
}
