import java.io.IOException;

import java.net.URL;
import stopwatch.StopWatch;

/**
 * Application of state machine.
 * @author Salilthip 5710546640
 *
 */
public class Main {

	public static void main(String[]args) throws IOException{
		
		StopWatch watch = new StopWatch();
		
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt"; 
		URL url = new URL( DICT_URL ); 
		WordCounter counter = new WordCounter(); 
		watch.start();
		int wordcount = counter.countWords( url ); 
		int syllables = counter.getSyllableCount( );
		watch.stop();

		System.out.println("Reading words from http://se.cpe.ku.ac.th/dictionary.txt");
		System.out.printf("Counted %d syllables in %d words\n",syllables,wordcount);
		System.out.printf("Elapsed time: %.3f sec",watch.getElapsed());


	}

}
