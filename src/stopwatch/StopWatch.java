package stopwatch;

/**
 * StopWatch that measure elapsed time between stop and stop time.
 */
public class StopWatch {
	
	private long startTime=0;
	private long stopTime=0;
	private static final double NANOSECONDS = 1.0E-9;
	private boolean running=false;

	
	/**
	 * Get elapsed time
	 * @return the elapsed time since start until the current time ,if called while stopwatch is running.
		 		the time between the start and stop times ,if stopwatch is stopped.
	 */
	public double getElapsed(){
		long elapse=0;
		if(running){
			elapse = (System.nanoTime()-startTime);
		}
		else{
			elapse = (stopTime-startTime);
		}
		return (elapse)*NANOSECONDS;
	}
	
	
	/**
	 * Check status of stopwatch.
	 * @return	true if the stopwatch is running,
	 * 			false if stopwatch is stopped.
	 */
	public boolean isRunning(){
		if(running==true)
		return true;
		else
		return false;
	}
	
	/**
	 *  reset the timer and start the stopwatch if stopwatch is not running.
	 *	If the stopwatch is alreadyrunning then start does nothing.
	 */
	public void start(){
		if(!isRunning()){
		startTime=System.nanoTime();
		}
		running=true;
	}
	
	/**
	 *  stop the stopwatch. If the stopwatch is already stopped, then stop does nothing.
	 */
	public void stop(){
		if(isRunning()){
		stopTime=System.nanoTime();
		}
			running=false;
	}
	
}